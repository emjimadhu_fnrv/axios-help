import service from '@/services'
import router from '@/router'

const state = {
  token: null
}

const getters = {
  token: (state) => state.token
}

const mutations = {
  SET_TOKEN: (state, payload) => {
    state.token = payload
  }
}

const actions = {
  login ({commit}, payload) {
    service.login()
      .then((success) => {
        commit('SET_TOKEN', success.data.token)
        router.push({
          name: 'protected'
        })
      })
      .catch((error) => {
        console.log(error)
      })
  }
}

export default {
  state, getters, mutations, actions
}
