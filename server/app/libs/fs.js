const { promisify } = require('util')
const fs = require('fs')

const read = promisify(fs.readFile)
module.exports.read = read

const write = promisify(fs.writeFile)
module.exports.write = write
