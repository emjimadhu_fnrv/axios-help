import axios from 'axios'

export const api = () => {
  return axios.create({
    baseURL: 'http://localhost:3000'
  })
}

export const apiHeaders = (token) => {
  return axios.create({
    baseURL: 'http://localhost:3000',
    headers: {
      'Authorization': token
    }
  })
}
