import { api, apiHeaders } from '@/services/api'

export default {
  login () {
    return api().get('/login')
  },
  protected (token) {
    return apiHeaders(token).get('/protected')
  }
}
