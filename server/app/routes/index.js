const uuid = require('uuid/v1')
const path = require('path')
const { read, write } = require(__base + 'libs/fs')

module.exports = (app) => {
  app.get('/',
    (req, res) => {
      res.status(200).send({
        message: 'Home Page'
      })
    })

  app.get('/login',
    (req, res) => {
      const token = {
        token: uuid()
      }

      write(path.join(__base + 'files/token.json'), JSON.stringify(token, null, 2))
        .then(() => {
          res.status(200).send({
            token: token
          })
        })
        .catch((error) => {
          res.send({
            error: 'Error when Writing File',
            stack: error
          })
        })

    })

app.get('/protected',
  (req, res, next) => {
    if (!req.headers.authorization) {
      return res.status(403).send({ error: 'No credentials sent!' });
    }
    next()
  },
  (req, res) => {
    read(__base + 'files/token.json', 'utf8')
      .then((data) => {
        if (JSON.parse(data).token === req.headers.authorization) {
          res.status(200).send({
            message: 'Token Valid',
            headers: req.headers
          })
        } else {
          res.status(401).send({
            message: 'Token Invalid'
          })
        }
      })
      .catch((error) => {
        res.send({
          error: 'Error when Reading File',
          stack: error
        })
      })
  })
}
