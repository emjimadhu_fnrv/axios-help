import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Protected from '@/components/Protected'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'login',
      component: HelloWorld
    },
    {
      path: '/protected',
      name: 'protected',
      component: Protected
    }
  ]
})
