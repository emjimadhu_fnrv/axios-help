global.__base = __dirname + '/'

const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors())

require(__base + 'routes')(app)

app.listen(3000, () => {
  console.log('Server is running on http://localhost:3000')
})
